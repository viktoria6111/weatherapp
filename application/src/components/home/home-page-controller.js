module.exports = function(moduleName){
  moduleName.controller('MainPageController', ['$scope', 'AppService', 'GenericServices', 'OPEN_WEATHER_KEY',
    function ($scope, AppService, GenericServices, OPEN_WEATHER_KEY) {

      $scope.genericServices = GenericServices;
      $scope.weatherData = [];
      $scope.counter = 12;
      $scope.forecast = [];
      $scope.city = {name: ''};
      $scope.findedCity = [];
			$scope.citiesIDs = [];
      $scope.coordinates =
        {
          latitude: 50.4,
          longitude: 30.5
        };

			/**
			 * Get weather data by coordinates
			 */
      AppService.getWeather($scope.coordinates.latitude, $scope.coordinates.longitude, $scope.counter, OPEN_WEATHER_KEY)
        .then(function (data) {
          $scope.weatherData = data;
          $scope.weatherDataLength = $scope.weatherData.data.list.length;
					$scope.citiesIDs = $scope.genericServices.getIds($scope.weatherData.data.list);
      })
        .then(function () {
          AppService.getFutureWeather($scope.weatherData.data.list[0].id, OPEN_WEATHER_KEY).then(function (data) {
            $scope.forecast = data;
						$scope.genericServices.markCheckedCities($scope.citiesIDs);
          })
      });

			/**
			 * Find weather by city name and cities around
			 */
      $scope.findCity = function () {
        AppService.getWeatherByCityName($scope.city.name, OPEN_WEATHER_KEY)
          .then(function (data) {
            $scope.findedCity = data;
            $scope.coordinates.latitude = $scope.findedCity.data.coord.lat;
            $scope.coordinates.longitude = $scope.findedCity.data.coord.lon;
          })
          .then(function () {
            AppService.getWeather($scope.coordinates.latitude, $scope.coordinates.longitude, $scope.counter, OPEN_WEATHER_KEY)
              .then(function (data) {
                $scope.weatherData = data;
              })
          });
      };

			/**
			 * Find forecast by city id
			 */
      $scope.getForecast = function (id) {
        AppService.getFutureWeather(id, OPEN_WEATHER_KEY).then(function (data) {
          $scope.forecast = data;
        })
      };

			/**
			 * Get 12 cities when button "Load more" clicked
			 */
      $scope.getMoreCities = function() {
        GenericServices.checkArrayLength(
          $scope.weatherDataLength, $scope.counter, function () {
            $scope.counter += 12;

            AppService.getWeather($scope.coordinates.latitude, $scope.coordinates.longitude, $scope.counter, OPEN_WEATHER_KEY)
              .then(function (data) { 
                $scope.weatherData = data;
                $scope.weatherDataLength = $scope.weatherData.data.list.length;
            });
          }
        )
      };

			/**
			 * Save city id in local storage
			 */
      $scope.saveCity = function (id, data) {
        $scope.genericServices.saveForCompare(id, data);
      }
    }
  ])
};