module.exports = function(moduleName){
  moduleName.controller('FavoritesController', ['$scope', 'AppService', 'GenericServices', 'OPEN_WEATHER_KEY',
    
    function ($scope, AppService, GenericServices, OPEN_WEATHER_KEY) {
      $scope.compareData = [];
      $scope.generic = GenericServices;
      $scope.itemsForCompare = $scope.generic.getCitiesIds();
      $scope.propertyName = 'city.data.city.name';
      $scope.reverse = true;

      $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
      };
      
      $scope.removeItem = function (id, item) {
        $scope.generic.removeFromFavorites(id);
        var index = $scope.compareData.indexOf(item);
        $scope.compareData.splice(index, 1);
      };

      $scope.getComparingData = function () {
        var data = $scope.itemsForCompare;
        for (var i = 0; i < data.length; i++){
          AppService.getFutureWeather(data[i], OPEN_WEATHER_KEY).then(function (data) {
            $scope.compareData.push(data);
          });
        }
      };

      $scope.getComparingData();
    }
  ])
};
