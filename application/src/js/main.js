var weatherApp = angular.module('WeatherApp', ['ngRoute']);

weatherApp.config(['$routeProvider', function ($routeProvider) {
  $routeProvider.
    when('/home', {
      templateUrl: 'components/home/home.html',
      controller: 'MainPageController'
    }).
    when('/favorites', {
      templateUrl: 'components/favorites/favorites.html',
      controller: 'FavoritesController'
    }).
    otherwise({
      redirectTo: '/home'
    });
}]);

weatherApp.constant('OPEN_WEATHER_KEY', '6bcfc50c216f92e57c5de18d5eb123a6');

weatherApp.filter('trustUrl', function ($sce) {
  return $sce.trustAsResourceUrl;
});

require('../shared/app-service.js')(weatherApp);
require('./../shared/generic-services.js')(weatherApp);
require('../components/home/home-page-controller.js')(weatherApp);
require('../components/favorites/favorites.js')(weatherApp);