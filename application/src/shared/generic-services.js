module.exports = function (moduleName) {
  moduleName.factory('GenericServices', function () {
    return {
      /**
       * Save element id in LS if element is checked
       * @param id - id of item
       */
      saveForCompare: function (id) {
        var savedCities = localStorage.getItem('weatherFavorites');

        savedCities = savedCities ? savedCities.split(',') : [];
        savedCities.push(id);
        localStorage.setItem('weatherFavorites', savedCities.join(','));
      },
      /**
       * Get id`s from LS
       * @returns {Array}
       */
      getCitiesIds: function () {
        var savedCities = localStorage.getItem('weatherFavorites');
        return savedCities ? savedCities.split(',') : [];
      },
      /**
       * Check  length of data in scope
       * @param arrayLength
       * @param counter
       * @param fn
       */
      checkArrayLength: function (arrayLength, counter, fn) {
        var button = document.getElementById('loadMore');
        if(arrayLength < counter){
          button.className = 'inactive';
        }else {
          fn();
        }
      },
      markCheckedCities: function (data) {
        var checkedCities = this.getCitiesIds();
        for(var i = 0; i < data.length; i++){
          if(checkedCities.indexOf(data[i] + '') > -1) {
            var index = 'item_' + data[i];
            document.getElementById(index).className += 'checked-item';
          }
        }
      },
      getIds: function (data) {
        var idArray = [];
        for(var i = 0; i < data.length; i++){
          idArray.push(data[i].id);
        }
        return idArray;
      },
      /**
       * Remove city id from LS
       * @param id - city id
       */
      removeFromFavorites: function (id) {
        var citiesIds = this.getCitiesIds();
        var ids = [];
        for(var i = 0; i < citiesIds.length; i++) {
          if(id == citiesIds[i]){
          }else {
            ids.push(citiesIds[i]);
          }
        }
        localStorage.setItem('weatherFavorites', ids.join(','));
      },
      /**
       * Transformation deg to direction
       * @param {number} data - wind direction
       * @returns {String}
       */
      getWindDirection: function (data) {
        var direction;
        switch (true){
          case data <= 45:
            direction = 'North' + ' (' + data + ')';
            break;
          case data <= 90:
            direction = 'Northeast' + ' (' + data + ')';
            break;
          case data <= 135:
            direction = 'East' + ' (' + data + ')';
            break;
          case data <= 180:
            direction = 'Southeast' + ' (' + data + ')';
            break;
          case data <= 225:
            direction = 'South' + ' (' + data + ')';
            break;
          case data <= 270:
            direction = 'West' + ' (' + data + ')';
            break;
          case data <= 315:
            direction = 'Northwest' + ' (' + data + ')';
            break;
          case data <= 360:
            direction = 'North' + ' (' + data + ')';
            break;
        }
        return direction;
      },
      /**
       * Helper function
       * @param fn
       * @param input
       * @returns {*}
       */
      transformer: function (fn, input) {
        return (fn || angular.identity)(input)
      },
      /**
       * Helper function
       * @param data
       * @returns {string}
       */
      sliceNumber: function (data) {
        return data.toFixed();
      },
      /**
       * Helper function
       * @param date
       * @returns {string}
       */
      changeDataView: function (date) {
        var date = new Date(date),
          day = date.getDate(),
          monthNames = ["Jan", "Feb", "March", "April", "May", "June",
          "July", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];

        return day + ' ' + monthNames[date.getMonth()];
      }
    }
  })
};