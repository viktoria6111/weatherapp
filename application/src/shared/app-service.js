module.exports = function (moduleName) {
  moduleName.service('AppService', ['$http', function ($http) {

    this.getWeather = function (latitude, longitude, cout, openWeatherKey) {
      var requestString = 'http://api.openweathermap.org/data/2.5/find?'
        + 'lat=' + latitude
        + '&lon=' + longitude
        + '&cnt=' + cout
        + '&format=json&units=metric'
				+	'&cluster=no'
        + '&APPID=' + openWeatherKey;

      return $http.get(requestString);
    };

		this.getFutureWeather = function (id, openWeatherKey) {
			var requestString = 'http://api.openweathermap.org/data/2.5/forecast?'
					+ 'id=' + id
					+ '&units=metric'
					+	'&cluster=yes'
				  + '&APPID=' + openWeatherKey;
			
			return $http.get(requestString);
		};
		
		this.getWeatherByCityName = function (name, openWeatherKey) {
			var requestString = 'http://api.openweathermap.org/data/2.5/weather?'
				+ 'q=' + name
				+ '&units=metric'
				+ '&APPID=' + openWeatherKey;

			return $http.get(requestString);
		}
  }])
};