module.exports = function(grunt) {
  grunt.initConfig({

    sass: {
      dist: {
        files: {
          './dist/public/application/css/app.css': [ 
            'src/sass/main.scss',
            'src/sass/header.scss',
            'src/sass/home-page.scss', 
            'src/sass/favorites.sccs'
          ]
        }
      }
    },
    clean: {
      all: {
        src: ['./dist/public/application']
      },
      css: {
        src: ['./dist/public/application/css']
      },
      scripts: {
        src: ['./dist/public/application/js']
      },
      html: {
        src: ['./dist/public/application/**/*.html']
      }
    },
    copy: {
      html: {
        files: [
          {
            expand: true,
            cwd: './src',
            src: ['**/*.html'],
            dest: './dist/public/application/'
          }
        ]
      }, 
        libs: {
            files: [
                {
                    expand: true,
                    cwd: './src/libs',
                    src: ['**/*'],
                    dest: './dist/public/application/www/libs'
                }
            ]
        }
    },
    browserify: {
      dist: {
        files: {'./dist/public/application/js/index.js': './src/**/main.js'}
      }
    },
    watch: {
      sass: {
        files: ['./src/**/*.scss'],
        tasks: ['clean:css', 'sass']
      },
      scripts: {
        files: ['./src/**/*.js'],
        tasks: ['clean:scripts', 'browserify']
      },
      html: {
        files: ['./src/**/*.html'],
        tasks: ['clean:html', 'copy']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  // Default task
  grunt.registerTask('default', ['clean:all', 'sass', 'copy', 'browserify']);
	// Watch task 
  grunt.registerTask('watch', ['clean:all', 'sass', 'copy', 'browserify', 'watch']);
};
