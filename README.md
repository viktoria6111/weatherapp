# Weather App 
---------------
####Version 1.0

## Usage

Clone a copy of the git repo by running:
  
* git clone git@bitbucket.org:viktoria6111/weatherapp.git

Enter the 'weatherApp/application' directory and run:

  * npm install
  * npm run build

Enter the dist directory and run index.html